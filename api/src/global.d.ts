declare namespace NodeJS {
    interface ProcessEnv {
        PORT: string
        UI_URL: string
        NODE_ENV: string
        STATIC_USER: string
        STATIC_PASSWORD: string
        STATIC_TOKEN: string
    }
}