import { UserLoginRequestDto } from "./user-login-request-dto";
import { UserLoginResponseDto } from "./user-login-response-dto";
import { ValidationError } from "@application/validation-error";



export default (dto: UserLoginRequestDto): UserLoginResponseDto => {
    if (dto.name != process.env.STATIC_USER || dto.password != process.env.STATIC_PASSWORD) {
        throw new ValidationError("Invalid Credentials");
    }

    const response = new UserLoginResponseDto(process.env.STATIC_TOKEN);
    return response;
}